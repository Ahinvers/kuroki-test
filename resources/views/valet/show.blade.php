@extends('scaffold-interface.layouts.app')
@section('title','Show')
@section('content')

<section class="content">
    <h1>
        Show valet
    </h1>
    <br>
    <a href='{!!url("valet")!!}' class = 'btn btn-primary'><i class="fa fa-home"></i>Valet Index</a>
    <br>
    <table class = 'table table-bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td> <b>Rut</b> </td>
                <td>{!!$valet->Rut!!}</td>
            </tr>
            <tr>
                <td> <b>Nombre</b> </td>
                <td>{!!$valet->Nombre!!}</td>
            </tr>
            <tr>
                <td> <b>Contacto</b> </td>
                <td>{!!$valet->Contacto!!}</td>
            </tr>
        </tbody>
    </table>
</section>
@endsection