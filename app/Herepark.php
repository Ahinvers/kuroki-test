<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Herepark.
 *
 * @author  The scaffold-interface created at 2020-09-20 10:38:51pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Herepark extends Model
{
	
	
    protected $table = 'hereparks';

	
	public function valet()
	{
		return $this->belongsTo('App\Valet','valet_id');
	}

	
	public function vehiculo()
	{
		return $this->belongsTo('App\Vehiculo','vehiculo_id');
	}

	
}
