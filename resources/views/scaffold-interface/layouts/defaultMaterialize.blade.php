<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/css/materialize.min.css">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<!--Let browser know website is optimized for mobile-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>@yield('title')</title>
	</head>
	<body>
		<div class="navbar-fixed">
	<nav>
<ul id="authdropdown" class="dropdown-content darken-4 learn-nav">
	<li><a href="{{ url('/logout') }}">Logout</a></li>
</ul>
		<div class="nav-wrapper">
			<div class="container">
				<a href="{{ url('/') }}" class="flow-text">Herepark</a>
			 <ul class="right hide-on-med-and-down">
				@if (Auth::guest())
						<li><a href="{{ url('/login') }}">Login</a></li>
						<li><a href="{{ url('/register') }}">Register</a></li>
				@else
						<li>
								<a class="dropdown-button" data-activates="authdropdown" href="#">
										{{ Auth::user()->name }}
										<i class="material-icons right">arrow_drop_down</i>
								</a>
						</li>
				@endif
			 </ul>

			 <ul id="nav-mobile" class="side-nav">
				@if (Auth::guest())
						<li><a href="{{ url('/login') }}">Login</a></li>
						<li><a href="{{ url('/register') }}">Register</a></li>
				@else
						<li><a href="{{ url('/logout') }}">Logout</a></li>
				@endif
			</ul>
		</div>
	</div>
</nav>
</div>

@yield('content')

<!-- JavaScripts -->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
<script>
	$(document).ready(function () {
		$(".dropdown-button").dropdown();
		$(".button-collapse").sideNav();
	});
</script>
		<div id="modal1" class="modal">
			<div class = "row AjaxisModal">
			</div>
		</div>
		<script src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script src = "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/js/materialize.min.js"></script>
		<script>  var baseURL = "{{ URL::to('/') }}"</script>
		<script src = "{{URL::asset('js/AjaxisMaterialize.js')}}"></script>
		<script src = "{{URL::asset('js/scaffold-interface-js/customA.js')}}"></script>
		<script>
		$('select').material_select();
		</script>
	</body>
</html>
