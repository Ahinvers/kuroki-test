<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Valet.
 *
 * @author  The scaffold-interface created at 2020-09-20 10:30:18pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Valet extends Model
{
	
	
    public $timestamps = false;
    
    protected $table = 'valets';

	
}
