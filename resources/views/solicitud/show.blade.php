@extends('scaffold-interface.layouts.app')
@section('title','Show')
@section('content')

<section class="content">
    <h1>
        Show solicitud
    </h1>
    <br>
    <a href='{!!url("solicitud")!!}' class = 'btn btn-primary'><i class="fa fa-home"></i>Solicitud Index</a>
    <br>
    <table class = 'table table-bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>Patente : </i></b>
                </td>
                <td>{!!$solicitud->vehiculo->Patente!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Marca : </i></b>
                </td>
                <td>{!!$solicitud->vehiculo->Marca!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Modelo : </i></b>
                </td>
                <td>{!!$solicitud->vehiculo->Modelo!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Comentario : </i></b>
                </td>
                <td>{!!$solicitud->vehiculo->Comentario!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Nombre : </i></b>
                </td>
                <td>{!!$solicitud->vehiculo->Nombre!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Contacto : </i></b>
                </td>
                <td>{!!$solicitud->vehiculo->Contacto!!}</td>
            </tr>
        </tbody>
    </table>
</section>
@endsection