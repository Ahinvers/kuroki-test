<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vehiculo;

class SearchController extends Controller
{
    //función para autocompletar patentes
    public function patentes(Request $request){
        //variable para almacenar lo que escribe el usuario
        $term = $request->get('term');
        //resultados de la busqueda
        $querys = Vehiculo::where('patente', 'LIKE', '%'.$term.'%')->get();
        //crear arreglo para almacenar ressultados
        $data = [];
        foreach($querys as $query){
            //guardamos solo el resultado especifico de la patente
            $data[] = [
                'label' => $query->Patente 
            ];
        }
        return $data;
    }
    public function marcas(Request $request){
        $term = $request->get('term');
        $querys = Vehiculo::where('marca', 'LIKE', '%'.$term.'%')->get();
        $data = [];
        foreach($querys as $query){
            $data[] = [
                'label' => $query->Marca 
            ];
        }
        return $data;
    }
}
