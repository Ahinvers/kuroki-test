@extends('scaffold-interface.layouts.app')
@section('title','Edit')
@section('content')

<section class="content">
    <h1>
        Editar Registro
    </h1>
    <a href="{!!url('valet')!!}" class = 'btn btn-primary'><i class="fa fa-home"></i> Listado de Valets</a>
    <br>
    <form method = 'POST' action = '{!! url("valet")!!}/{!!$valet->
        id!!}/update'>
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="form-group">
            <label for="Rut">Rut</label>
            <input id="Rut" name = "Rut" type="text" class="form-control" value="{!!$valet->
            Rut!!}">
        </div>
        <div class="form-group">
            <label for="Nombre">Nombre</label>
            <input id="Nombre" name = "Nombre" type="text" class="form-control" value="{!!$valet->
            Nombre!!}">
        </div>
        <div class="form-group">
            <label for="Contacto">Contacto</label>
            <input id="Contacto" name = "Contacto" type="text" class="form-control" value="{!!$valet->
            Contacto!!}">
        </div>
        <button class = 'btn btn-success' type ='submit'><i class="fa fa-floppy-o"></i> Actualizar</button>
    </form>
</section>
@endsection
