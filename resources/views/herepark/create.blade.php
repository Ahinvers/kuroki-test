@extends('scaffold-interface.layouts.appTest')
@section('title','Create')
@section('content')

<section class="content">
    <h1>
        Registar Estacionamiento
    </h1>
    <a href="{!!url('herepark')!!}" class = 'btn btn-danger'><i class="fa fa-home"></i> Listado de Estacionamiento</a>
    <br>
    <form method = 'POST' action = '{!!url("herepark")!!}'>
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="form-group">
            <label for="Numero">Numero</label>
            <input id="Numero" name = "Numero" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label for="Lugar">Lugar</label>
            <input id="Lugar" name = "Lugar" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label>Selecciona valet</label>
            <select name = 'valet_id' class = 'form-control'>
                @foreach($valets as $key => $value)
                <option value="{{$key}}">{{$value}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="vehiculo_id">Vehículo</label>
            <input id="vehiculo_id" name = "vehiculo_id" type="text" class="form-control">
        </div>
        <button class = 'btn btn-success' type ='submit'> <i class="fa fa-floppy-o"></i> Guardar</button>
    </form>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="{{asset('jquery-ui/jquery-ui.min.js')}}"></script>
<script>
//tomamos el id del campo y utilizamos la funcion autocomplete
    $('#vehiculo_id').autocomplete({
        source: function(request, response){
            $.ajax({
                //asignamos la ruta
                url: "{{route('search.vehiculos')}}",
                //asignamos el tipo de dato
                dataType: 'json',
                //variable [data] en el controlador
                data: {
                    term: request.term
                },
                //si todo salió bien, nos devuelve los resultados
                success: function(data){
                    response(data)
                }
            });
        }
    });
        $('#Marca').autocomplete({
        source: function(request, response){
            $.ajax({
                url: "{{route('search.marcas')}}",
                dataType: 'json',
                data: {
                    term: request.term
                },
                success: function(data){
                    response(data)
                }
            });
        }
    });
</script>
@endsection
