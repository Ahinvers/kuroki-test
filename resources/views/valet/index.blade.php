@extends('scaffold-interface.layouts.appTest')
@section('title','Index')
@section('content')

<section class="content">
    <h1>
        Listado de Valets
    </h1>
    <a href='{!!url("valet")!!}/create' class = 'btn btn-success'><i class="fa fa-plus"></i> Registar Valet</a>
    <br>
    <br>
    <table id="test" class = "table table-responsive" style = 'background:#fff'>
        <thead>
            <th>Rut</th>
            <th>Nombre</th>
            <th>Contacto</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($valets as $valet)
            <tr>
                <td>{!!$valet->Rut!!}</td>
                <td>{!!$valet->Nombre!!}</td>
                <td>{!!$valet->Contacto!!}</td>
                <td>
                    <a data-toggle="modal" data-target="#myModal" class = 'delete btn btn-danger btn-xs' data-link = "/valet/{!!$valet->id!!}/deleteMsg" ><i class = 'fa fa-trash'> delete</i></a>
                    <a href = '#' class = 'viewEdit btn btn-primary btn-xs' data-link = '/valet/{!!$valet->id!!}/edit'><i class = 'fa fa-edit'> edit</i></a>
                    <a href = '#' class = 'viewShow btn btn-warning btn-xs' data-link = '/valet/{!!$valet->id!!}'><i class = 'fa fa-eye'> info</i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {!! $valets->render() !!}

</section>
@endsection
