@extends('scaffold-interface.layouts.appTest')
@section('title','Create')
@section('content')

<section class="content">
    <h1>
        Registrar vehiculo
    </h1>
    <a href="{!!url('vehiculo')!!}" class = 'btn btn-danger'><i class="fa fa-home"></i> Listado Vehiculos</a>
    <br>
    <form method = 'POST' action = '{!!url("vehiculo")!!}'>
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="form-group">
            <label for="Patente">Patente</label>
            <input id="Patente" name = "Patente" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label for="Marca">Marca</label>
            <input id="Marca" name = "Marca" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label for="Modelo">Modelo</label>
            <input id="Modelo" name = "Modelo" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label for="Comentario">Comentario</label>
            <input id="Comentario" name = "Comentario" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label for="Nombre">Nombre</label>
            <input id="Nombre" name = "Nombre" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label for="Contacto">Contacto</label>
            <input id="Contacto" name = "Contacto" type="text" class="form-control">
        </div>
        <button class = 'btn btn-success' type ='submit'> <i class="fa fa-floppy-o"></i> Guardar</button>
    </form>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="{{asset('jquery-ui/jquery-ui.min.js')}}"></script>
<script>
//tomamos el id del campo y utilizamos la funcion autocomplete
    $('#Patente').autocomplete({
        source: function(request, response){
            $.ajax({
                //asignamos la ruta
                url: "{{route('search.patentes')}}",
                //asignamos el tipo de dato
                dataType: 'json',
                //variable [data] en el controlador
                data: {
                    term: request.term
                },
                //si todo salió bien, nos devuelve los resultados
                success: function(data){
                    response(data)
                }
            });
        }
    });
        $('#Marca').autocomplete({
        source: function(request, response){
            $.ajax({
                url: "{{route('search.marcas')}}",
                dataType: 'json',
                data: {
                    term: request.term
                },
                success: function(data){
                    response(data)
                }
            });
        }
    });
</script>
@endsection
