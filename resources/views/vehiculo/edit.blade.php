@extends('scaffold-interface.layouts.appTest')
@section('title','Edit')
@section('content')

<section class="content">
    <h1>
      Editar Registros del vehiculo   vehiculo
    </h1>
    <a href="{!!url('vehiculo')!!}" class = 'btn btn-primary'><i class="fa fa-home"></i> Vehiculo Index</a>
    <br>
    <form method = 'POST' action = '{!! url("vehiculo")!!}/{!!$vehiculo->
        id!!}/update'>
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="form-group">
            <label for="Patente">Patente</label>
            <input id="Patente" name = "Patente" type="text" class="form-control" value="{!!$vehiculo->
            Patente!!}">
        </div>
        <div class="form-group">
            <label for="Marca">Marca</label>
            <input id="Marca" name = "Marca" type="text" class="form-control" value="{!!$vehiculo->
            Marca!!}">
        </div>
        <div class="form-group">
            <label for="Modelo">Modelo</label>
            <input id="Modelo" name = "Modelo" type="text" class="form-control" value="{!!$vehiculo->
            Modelo!!}">
        </div>
        <div class="form-group">
            <label for="Comentario">Comentario</label>
            <input id="Comentario" name = "Comentario" type="text" class="form-control" value="{!!$vehiculo->
            Comentario!!}">
        </div>
        <div class="form-group">
            <label for="Nombre">Nombre</label>
            <input id="Nombre" name = "Nombre" type="text" class="form-control" value="{!!$vehiculo->
            Nombre!!}">
        </div>
        <div class="form-group">
            <label for="Contacto">Contacto</label>
            <input id="Contacto" name = "Contacto" type="text" class="form-control" value="{!!$vehiculo->
            Contacto!!}">
        </div>
        <button class = 'btn btn-success' type ='submit'><i class="fa fa-floppy-o"></i> Actualizar</button>
    </form>
</section>
@endsection
