@extends('scaffold-interface.layouts.appTest')
@section('title','Index')
@section('content')

<section class="content">
    <h1>
        Listado De vehiculos
    </h1>
    <a href='{!!url("vehiculo")!!}/create' class = 'btn btn-success'><i class="fa fa-plus"></i> Registar vehiculo</a>
    <br>
    <br>
<table id="test" class = "table table-responsive" style = 'background:#fff'>
        <thead>
            <th>Patente</th>
            <th>Marca</th>
            <th>Modelo</th>
            <th>Comentario</th>
            <th>Nombre</th>
            <th>Contacto</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($vehiculos as $vehiculo)
            <tr>
                <td>{!!$vehiculo->Patente!!}</td>
                <td>{!!$vehiculo->Marca!!}</td>
                <td>{!!$vehiculo->Modelo!!}</td>
                <td>{!!$vehiculo->Comentario!!}</td>
                <td>{!!$vehiculo->Nombre!!}</td>
                <td>{!!$vehiculo->Contacto!!}</td>
                <td>
                    <a href = '#' class = 'viewEdit btn btn-primary btn-xs' data-link = '/vehiculo/{!!$vehiculo->id!!}/edit'><i class = 'fa fa-edit'> edit</i></a>
                    <a href = '#' class = 'viewShow btn btn-warning btn-xs' data-link = '/vehiculo/{!!$vehiculo->id!!}'><i class = 'fa fa-eye'> info</i></a>
                    <a href = '#' class = 'viewShow btn btn-primary btn-xs' href='{!!url("herepark")!!}/create'><i class = 'fa fa-eye'> Estacionamiento</i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {!! $vehiculos->render() !!}

</section>
@endsection
